<?php
/**
 * Created by PhpStorm.
 * User: Dom
 * Date: 29.09.2018
 * Time: 15:57
 */

namespace App\Controller;

use App\Entity\Todo;
use App\Repository\TodoRepository;
use PhpParser\Node\Expr\Array_;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class ToDoController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('todo/homepage.html.twig');
    }

    /**
     * @Route("/todos", name="todos_list")
     */
    public function listAction()
    {
        $todos = $this->getDoctrine()->getRepository('App\Entity\Todo')->findAll();
        return $this->render('todo/list.html.twig', array('todos' => $todos));
    }

    /**
     * @Route("/todos/create", name="todos_create")
     */
    public function createAction(Request $request)
    {
        $todo =  new Todo();
        $todo->setCreateDate(new \DateTime('today'));
        $form = $this->createFormBuilder($todo)
            ->add('name', TextType::class)
            ->add('category', TextType::class)
            ->add('description', TextareaType::class)
            ->add('priority', ChoiceType::class, array('choices' => array('Low' => 'Low', 'Normal' => 'Normal', 'High' => 'High')))
            ->add('dueDate', DateTimeType::class, array(
                'placeholder' => array(
                'year' => 'Year', 'month' => 'Month', 'day' => 'Day',)))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todo);
            $entityManager->flush();

            return $this->redirectToRoute('todos_list');
        }
        else{
            return $this->render('todo/create.html.twig', array('form' => $form-> createView()));
        }
    }

    /**
     * @Route("/todos/edit/{id}", name="todos_edit")
     */
    public function editAction($id, Request $request)
    {
        $todo = $this->getDoctrine()->getRepository('App\Entity\Todo')->find($id);
        $todo->setCreateDate(new \DateTime('today'));
        $form = $this->createFormBuilder($todo)
            ->add('name', TextType::class)
            ->add('category', TextType::class)
            ->add('description', TextareaType::class)
            ->add('priority', ChoiceType::class, array('choices' => array('Low' => 'Low', 'Normal' => 'Normal', 'High' => 'High')))
            ->add('dueDate', DateTimeType::class, array(
                'placeholder' => array(
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',)))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todo);
            $entityManager->flush();

            return $this->redirectToRoute('todos_list');
        }
        else{
            return $this->render('todo/edit.html.twig', array('form' => $form-> createView()));
        }
}

    /**
     * @Route("/todos/details/{id}", name="todos_details")
     */
    public function detailsAction($id)
    {
        $todo = $this->getDoctrine()->getRepository('App\Entity\Todo')->find($id);
        return $this->render('todo/details.html.twig', array('todo' => $todo));
    }

    /**
     * @Route("/todos/delete/{id}", name="todos_delete")
     */
    public function deleteAction($id)
    {
        $todo = $this->getDoctrine()->getRepository('App\Entity\Todo')->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($todo);
        $entityManager->flush();
        return $this->redirectToRoute('todos_list');
    }

}